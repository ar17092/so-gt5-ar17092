#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdio.h>
#include <unistd.h>
#include <sys/wait.h>
#include <stdlib.h>

int main() {

  int pid, estado;
  char *comandoCP[] = {"cp", "carpeta1/file", "carpeta2"};
  char *env[] = {"TERM=xterm"};
  pid = fork();
  if (pid == -1) {
    printf("Error en fork\n");
    exit(-1);
  }else if (pid == 0) {
    execve("/usr/bin/cp", comandoCP, env);
    perror("Error en la llamada al sistema");
    exit(-1);
  }else{
    while (wait(&estado) != pid);
    (estado == 0)?printf("Ejecución normal \n se copió de la carpeta1 el archivo 'file'  a carpeta2\n"):printf("Error en la ejecución\n");
  }

  return 0;
}
